import os
import pip

pip.main(['install', 'pytelegrambotapi'])
import aiogram
from aiogram.dispatcher.filters import Text
from aiogram import Bot, Dispatcher, executor, types
from aiogram.types import ReplyKeyboardRemove, ReplyKeyboardMarkup, KeyboardButton
import sqlite3
import datetime as DT

API_TOKEN = '6268026158:AAH-d1kWvgtNzOXyrjLmCayu6R7tcHufhPQ'

bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot)

telegram_bot = sqlite3.connect('telegram_bot.db', check_same_thread=False)
cursor = telegram_bot.cursor()


def get_date():
  datetimeNow = telegram_bot.execute(
    f"SELECT change_num from change_list WHERE datetime='{DT.datetime.today().strftime('%Y-%m-%d')}'"
  ).fetchone()[0]
  if datetimeNow is None:
    return False
  else:
    return datetimeNow

def get_exact_date(date_num):
    try:
        datetimeNow = telegram_bot.execute(f"SELECT change_num from change_list WHERE datetime='2024-{date_num}'").fetchone()[0]
        if datetimeNow is None:
            return False
        else:
            return datetimeNow
    except:
        return False

@dp.message_handler(commands=['start'])
async def send_welcome(message: types.Message):
    kb = [
        [
            types.InlineKeyboardButton(text='Образец объяснительной')           
        ],
        [
            types.InlineKeyboardButton(text='Адреса общежитий')           
        ],
        [
            types.InlineKeyboardButton(text='Какая сегодня смена?')
        ],
        [
            types.InlineKeyboardButton(text='Узнать смену по дате')
        ],
        [
            types.InlineKeyboardButton(text='Списки общежитий')
        ]
      ]
    keyboard = types.ReplyKeyboardMarkup(keyboard=kb)
     
    await message.reply("Привет!\nЯ бот студотряда!\nЧто ты хочешь узнать?",  reply_markup=keyboard)


@dp.message_handler(Text("Образец объяснительной"))
async def example(message: types.Document):
  await message.reply_document(open('example.jpg', 'rb'))

@dp.message_handler(Text("образец объяснительной"))
async def example(message: types.Document):
  await message.reply_document(open('example.jpg', 'rb'))

@dp.message_handler(Text("Адреса общежитий"))
async def address(message: types.Message):
    await message.reply(f"Общежитие №2 - строение О\nОбщежитие №3 - строение H\nОбщежитие №4 - строение 1\nОбщежитие №5 - строение M")

@dp.message_handler(Text("адреса общежитий"))
async def address(message: types.Message):
    await message.reply(f"Общежитие №2 - строение О\nОбщежитие №3 - строение H\nОбщежитие №4 - строение 1\nОбщежитие №5 - строение M")

@dp.message_handler(Text("Какая сегодня смена?"))
async def change(message: types.Message):
  await message.reply(f"Сегодня {get_date()} смена")

@dp.message_handler(Text("какая сегодня смена?"))
async def change(message: types.Message):
  await message.reply(f"Сегодня {get_date()} смена")

@dp.message_handler(Text("Какая сегодня смена"))
async def change(message: types.Message):
  await message.reply(f"Сегодня {get_date()} смена")

@dp.message_handler(Text("какая сегодня смена"))
async def change(message: types.Message):
  await message.reply(f"Сегодня {get_date()} смена")

@dp.message_handler(Text("Какая смена?"))
async def change(message: types.Message):
  await message.reply(f"Сегодня {get_date()} смена")

@dp.message_handler(Text("какая смена?"))
async def change(message: types.Message):
  await message.reply(f"Сегодня {get_date()} смена")

@dp.message_handler(Text("Какая смена"))
async def change(message: types.Message):
  await message.reply(f"Сегодня {get_date()} смена")

@dp.message_handler(Text("какая смена"))
async def change(message: types.Message):
  await message.reply(f"Сегодня {get_date()} смена")

@dp.message_handler(Text("Чья смена?"))
async def change(message: types.Message):
  await message.reply(f"Сегодня {get_date()} смена")

@dp.message_handler(Text("чья смена?"))
async def change(message: types.Message):
  await message.reply(f"Сегодня {get_date()} смена")

@dp.message_handler(Text("Чья смена"))
async def change(message: types.Message):
  await message.reply(f"Сегодня {get_date()} смена")

@dp.message_handler(Text("чья смена"))
async def change(message: types.Message):
  await message.reply(f"Сегодня {get_date()} смена")

@dp.message_handler(Text("Чья сегодня смена?"))
async def change(message: types.Message):
  await message.reply(f"Сегодня {get_date()} смена")

@dp.message_handler(Text("чья сегодня смена?"))
async def change(message: types.Message):
  await message.reply(f"Сегодня {get_date()} смена")

@dp.message_handler(Text("Чья сегодня смена"))
async def change(message: types.Message):
  await message.reply(f"Сегодня {get_date()} смена")

@dp.message_handler(Text("чья сегодня смена"))
async def change(message: types.Message):
  await message.reply(f"Сегодня {get_date()} смена")

@dp.message_handler(Text("Списки общежитий"))
async def send_lists(message: types.Document):
  await types.ChatActions.upload_document()
  media = types.MediaGroup()
  media.attach_document(types.InputFile('#2.pdf'))
  media.attach_document(types.InputFile('#3.pdf'))
  media.attach_document(types.InputFile('#4.pdf'))
  media.attach_document(types.InputFile('#5.pdf'))
  await message.reply_media_group(media=media)

@dp.message_handler(Text("списки общежитий"))
async def send_lists(message: types.Document):
  await types.ChatActions.upload_document()
  media = types.MediaGroup()
  media.attach_document(types.InputFile('#2.pdf'))
  media.attach_document(types.InputFile('#3.pdf'))
  media.attach_document(types.InputFile('#4.pdf'))
  media.attach_document(types.InputFile('#5.pdf'))
  await message.reply_media_group(media=media)

@dp.message_handler(Text("Узнать смену по дате"))
async def asc_exatc_date(message: types.Message): 
    await message.reply("Введите месяц и день через тире\nНапример: 09-20")

@dp.message_handler()
async def asc_exatc_date(message: types.Message):
    if get_exact_date(message.text)!=False:
        await message.reply(f"{message.text} числа {get_exact_date(message.text)} смена")


if __name__ == '__main__':
  executor.start_polling(dp, skip_updates=True)
